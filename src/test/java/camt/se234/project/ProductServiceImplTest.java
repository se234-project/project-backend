package camt.se234.project;

import camt.se234.project.dao.ProductDaoImpl;
import camt.se234.project.entity.Product;
import camt.se234.project.repository.ProductRepository;
import camt.se234.project.service.ProductServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ProductServiceImplTest {
    ProductServiceImpl productService;
    ProductDaoImpl productDao;
    ProductRepository productRepository;
    @Before
    public void setup(){
        productService = new ProductServiceImpl();
        productDao = new ProductDaoImpl();
        productRepository = mock(ProductRepository.class);
        productDao.setProductRepository(productRepository);
        productService.setProductDao(productDao);
        List<Product> mockProducts = new ArrayList<>();
        mockProducts.add(new Product(001L,"p0001","Garden",
                "The garden which you can grow everything on earth",
                "garden.jpg",20000));
        mockProducts.add(new Product(002L,"p0002","Banana",
                "A good fruit with very cheap price",
                "banana.jpg",150));
        mockProducts.add(new Product(003L,"p0003","Orange",
                "Nothing good about it",
                "orange.jpg",280));
        mockProducts.add(new Product(004L,"p0004","Papaya",
                "Use for papaya salad",
                "papaya.jpg",12));
        mockProducts.add(new Product(005L,"p0005","Rambutan",
                "An expensive fruit from the sout",
                "rambutan.jpg",20));
        mockProducts.add(new Product(006L,"p0006","Unknow",
                "Don't use this",
                "rambutan.jpg",-1));
        when(productRepository.findAll()).thenReturn(mockProducts);
    }

    @Test
    public void testGetAllProductsMethod(){
        assertThat(productService.getAllProducts(),hasItems(
                new Product(001L,"p0001","Garden",
                        "The garden which you can grow everything on earth",
                        "garden.jpg",20000),
                new Product(002L,"p0002","Banana",
                        "A good fruit with very cheap price",
                        "banana.jpg",150),
                new Product(003L,"p0003","Orange",
                        "Nothing good about it",
                        "orange.jpg",280),
                new Product(004L,"p0004","Papaya",
                        "Use for papaya salad",
                        "papaya.jpg",12),
                new Product(005L,"p0005","Rambutan",
                        "An expensive fruit from the sout",
                        "rambutan.jpg",20),
                new Product(006L,"p0006","Unknow",
                        "Don't use this",
                        "rambutan.jpg",-1)));
    }

    @Test
    public void testGetAvailableProducts(){
        assertThat(productService.getAvailableProducts(),hasItems(
                new Product(001L,"p0001","Garden",
                        "The garden which you can grow everything on earth",
                        "garden.jpg",20000),
                new Product(002L,"p0002","Banana",
                        "A good fruit with very cheap price",
                        "banana.jpg",150),
                new Product(003L,"p0003","Orange",
                        "Nothing good about it",
                        "orange.jpg",280),
                new Product(004L,"p0004","Papaya",
                        "Use for papaya salad",
                        "papaya.jpg",12),
                new Product(005L,"p0005","Rambutan",
                        "An expensive fruit from the sout",
                        "rambutan.jpg",20)
                ));
    }

    @Test
    public void testGetUnavailableProductSize(){
       assertThat(productService.getUnavailableProductSize(),is(1));
    }
}
