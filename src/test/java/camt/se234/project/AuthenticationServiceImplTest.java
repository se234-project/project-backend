package camt.se234.project;

import camt.se234.project.dao.UserDaoImpl;
import camt.se234.project.entity.User;
import camt.se234.project.repository.UserRepositoryImpl;
import camt.se234.project.service.AuthenticationServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AuthenticationServiceImplTest {
    AuthenticationServiceImpl authenticationService;
    UserDaoImpl userDao;
    UserRepositoryImpl userRepository;

    @Before
    public void setup(){
        userRepository = mock(UserRepositoryImpl.class);
        userDao = new UserDaoImpl();
        userDao.setUserRepository(userRepository);
        authenticationService = new AuthenticationServiceImpl();
        authenticationService.setUserDao(userDao);
        List<User> mockUsers = new ArrayList<>();
        mockUsers.add(new User(001L,"admin","admin","admin"));
        mockUsers.add(new User(002L,"user","user","user"));
        mockUsers.add(new User(003L,"hello","hello","user"));
        when(userRepository.getUsers()).thenReturn(mockUsers);
    }

    @Test
    public void testAuthenticateMethodThatReturnsUserObject(){
        when(userRepository.findByUsernameAndPassword("admin","admin")).
                thenCallRealMethod();
        when(userRepository.findByUsernameAndPassword("user","user")).
                thenCallRealMethod();
        when(userRepository.findByUsernameAndPassword("hello","hello")).
                thenCallRealMethod();
        assertThat(authenticationService.authenticate("admin","admin"),
                is(new User(001L,"admin","admin","admin")));
        assertThat(authenticationService.authenticate("user","user"),
                is(new User(002L,"user","user","user")));
        assertThat(authenticationService.authenticate("hello","hello"),
                is(new User(003L,"hello","hello","user")));
    }

    @Test
    public void testAuthenticateMethodThatReturnsNull(){
        when(userRepository.findByUsernameAndPassword("null","null")).
                thenCallRealMethod();
        assertThat(authenticationService.authenticate("null","null"),
               nullValue());
    }
}
