package camt.se234.project;

import camt.se234.project.dao.OrderDaoImpl;
import camt.se234.project.entity.Product;
import camt.se234.project.entity.SaleOrder;
import camt.se234.project.entity.SaleTransaction;
import camt.se234.project.repository.OrderRepository;
import camt.se234.project.service.SaleOrderServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SaleOrderServiceImplTest {
    SaleOrderServiceImpl saleOrderService;
    OrderDaoImpl orderDao;
    OrderRepository orderRepository;
    SaleOrder saleOrderO001;
    SaleOrder saleOrderO002;

    @Before
    public void setup(){
        //Set up necessary objects
        saleOrderService = new SaleOrderServiceImpl();
        orderDao = new OrderDaoImpl();
        orderRepository = mock(OrderRepository.class);
        orderDao.setOrderRepository(orderRepository);
        saleOrderService.setOrderDao(orderDao);
        List<SaleOrder> mockSaleOrders = new ArrayList<>();
        saleOrderO001 = new SaleOrder();
        saleOrderO002 = new SaleOrder();
        List<SaleTransaction> transactionsForSaleOrderO001 = new ArrayList<>();
        List<SaleTransaction> transactionsForSaleOrderO002 = new ArrayList<>();
        Product productP0001 = new Product(001L,"p0001","Garden",
                "The garden which you can grow everything on earth",
                "garden.jpg",20000);
        Product productP0002 = new Product(002L,"p0002","Banana",
                "A good fruit with very cheap price",
                "banana.jpg",150);
        Product productP0004 = new Product(004L,"p0004","Papaya",
                "Use for papaya salad",
                "papaya.jpg",12);
        Product productP0005 = new Product(005L,"p0005","Rambutan",
                "An expensive fruit from the sout",
                "rambutan.jpg",20);

        //Set up saleOrderO001
        saleOrderO001.setId(001L);
        saleOrderO001.setSaleOrderId("o001");
        SaleTransaction saleTransactionT001 = new SaleTransaction(001L,"t001",saleOrderO001,productP0001,1);
        SaleTransaction saleTransactionT002 = new SaleTransaction(002L,"t002",saleOrderO001,productP0004,10);
        transactionsForSaleOrderO001.add(saleTransactionT001);
        transactionsForSaleOrderO001.add(saleTransactionT002);
        saleOrderO001.setTransactions(transactionsForSaleOrderO001);

        //Set up saleOrderO001
        saleOrderO002.setId(002L);
        saleOrderO002.setSaleOrderId("o002");
        SaleTransaction saleTransactionT003 = new SaleTransaction(003L,"t003",saleOrderO002,productP0002,2);
        SaleTransaction saleTransactionT004 = new SaleTransaction(004L,"t004",saleOrderO002,productP0001,3);
        SaleTransaction saleTransactionT005 = new SaleTransaction(005L,"t005",saleOrderO002,productP0002,1);
        SaleTransaction saleTransactionT006 = new SaleTransaction(006L,"t006",saleOrderO002,productP0005,6);
        transactionsForSaleOrderO002.add(saleTransactionT003);
        transactionsForSaleOrderO002.add(saleTransactionT004);
        transactionsForSaleOrderO002.add(saleTransactionT005);
        transactionsForSaleOrderO002.add(saleTransactionT006);
        saleOrderO002.setTransactions(transactionsForSaleOrderO002);

        //Add SaleOrder in array
        mockSaleOrders.add(saleOrderO001);
        mockSaleOrders.add(saleOrderO002);

        //set up orderRepository
        when(orderRepository.findAll()).thenReturn(mockSaleOrders);
    }

    @Test
    public void testGetSaleOrdersMethod(){
        assertThat(saleOrderService.getSaleOrders(),hasItems(saleOrderO001,saleOrderO002));
    }

    @Test
    public void testGetAverageSaleOrderPrice(){
        assertThat(saleOrderService.getAverageSaleOrderPrice(),closeTo(40345,01));
    }
}
