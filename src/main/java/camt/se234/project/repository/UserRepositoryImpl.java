package camt.se234.project.repository;

import camt.se234.project.entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepositoryImpl implements UserRepository{


    List<User> users;
    public List<User> getUsers() {
        return users;
    }

    public UserRepositoryImpl(){
        users = new ArrayList<>();
        users.add(new User(001L,"admin","admin","admin"));
        users.add(new User(002L,"user","user","user"));
        users.add(new User(003L,"hello","hello","user"));
    }

    @Override
    public User findByUsernameAndPassword(String username, String password) {
        for(User user: this.getUsers()){
            if(user.getUsername().equals(username)&&user.getPassword().equals(password))
                return user;
        }
        return null;
    }

    @Override
    public <S extends User> S save(S s) {
        return null;
    }

    @Override
    public <S extends User> Iterable<S> save(Iterable<S> iterable) {
        return null;
    }

    @Override
    public User findOne(Long aLong) {
        return null;
    }

    @Override
    public boolean exists(Long aLong) {
        return false;
    }

    @Override
    public Iterable<User> findAll() {
        return null;
    }

    @Override
    public Iterable<User> findAll(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void delete(Long aLong) {

    }

    @Override
    public void delete(User user) {

    }

    @Override
    public void delete(Iterable<? extends User> iterable) {

    }

    @Override
    public void deleteAll() {

    }
}
